#!/usr/bin/env python3
from datetime import datetime
import psutil

def add_or_update_server(slow_servers, node):
  if node in slow_servers:
    slow_servers[node] += 1
  else:
    slow_servers.update({ node: 1 })

  return slow_servers


def total_time(guid):
  if 'POST' in guid:
    method = 'POST'
  elif 'GET' in guid:
    method = 'GET'

  start_time = guid[method][0]
  end_time = guid['RESPOND'][0]

  return (end_time - start_time).microseconds/1000


def calculate_times(parsed_logs, slow_servers):
    for guid in parsed_logs:
      if 'POST' in parsed_logs[guid]:
        method = 'POST'
      elif 'GET' in parsed_logs[guid]:
        method = 'GET'

      start_time = parsed_logs[guid][method][0]
      worker_time = parsed_logs[guid]['HANDLE'][0]
      end_time = parsed_logs[guid]['RESPOND'][0]

      frontend_time = worker_time - start_time
      if frontend_time.microseconds/1000 > 250:
        node = parsed_logs[guid][method][1]
        if node in slow_servers:
          slow_servers[node] += 1
        else:
          slow_servers.update({ node: 1 })

      worker_duration = end_time - worker_time
      if worker_duration.microseconds/1000 > 250:
        node = parsed_logs[guid]['HANDLE'][1]
        if node in slow_servers:
          slow_servers[node] += 1
        else:
          slow_servers.update({ node: 1 })

if __name__ == "__main__":
  parsed_logs = {}
  slow_servers = {}
  mem_start = psutil.Process().memory_info().rss / (1024 * 1024)

  # 1. Parse log file and create a dictionary to work with afterwards
  with open('sample.log') as logfile:
    for line in logfile:
      date_time = datetime.strptime(line.split(' ')[0], "%Y-%m-%dT%H:%M:%S.%f")
      guid = line.split(' ')[1]
      method = line.split(' ')[2]
      server = line.split(' ')[-1].strip()
      entry = { guid: {
        method: [ date_time, server ] }
      }
      # 2 .Calculate total time
      if method == 'RESPOND':
        parsed_logs[guid].update(entry[guid])
        # if total time > x, do the server thing
        if total_time(parsed_logs[guid]) < 500:
          del parsed_logs[guid]
      else:
        # merge...
        if guid in parsed_logs:
          parsed_logs[guid].update(entry[guid])
        else:
          parsed_logs.update(entry)

  calculate_times(parsed_logs, slow_servers)

  # 3. print result
  print(slow_servers)
  mem_end = psutil.Process().memory_info().rss / (1024 * 1024)

  print(f'Start Memory: {mem_start}, End Memory: {mem_end}')
