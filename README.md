# Take Home Exercise

I imagine this would be used for a quick debug session, not something that will be reused. That's why I didn't implement any "convinience" features like arg parsing. I also wanted to keep the effort below 2hrs.
The main goal was to keep the memory requirement low so that it can handle large files as well.

## How to run

- Create a venv

  ```sh
  python3 -m venv .venv
  source .venv/bin/activate
  ```

- Install dependencies

  ```sh
  pip install -r requirements.txt
  ```

- Put log file named sample.log into the folder
- Run the script

  ```sh
  ./parse.py
  ```
